let server = "http://localhost:63206/";
let postURL = server + 'api/personas';
let getURL = server + 'api/personas'

function fnGuardarPersona() {
        data = {
                cedula: $('txt-cedula').val(),
                nombre: $('txt-nombre').val(),
                apellido1: $('txt-apellido1').val(),
                apellido2: $('txt-apellido2').val(),
                fechaNacimiento: $('txt-fecha-nacimiento').val(),
                sexo: $('.rd-sexo:checked').val(),
        }

        console.log(data);
        $.post(postURL, data)
                .done(function (data) {
                        console.log("Data Loaded: " + data);
                })
                .fail(function (data) {
                        console.log(data);
                });
}

function fnObtenerPersonas(){
        $.get(getURL).done(function (data) {
                console.log("Data Loaded: " + data);
        })
        .fail(function (data) {
                console.log(data);
        });
}